%define debug_package %{nil}
%define repo github.com/etesync/server-skeleton

Name:          etesync-server
Version:       0.3.0
Release:       1%{?dist}
Summary:       A self-hostable EteSync server

License:       AGPLv3
URL:           https://%{repo}
Source0:       https://%{repo}/archive/v%{version}.tar.gz

Requires:      python3-django-cors-headers python3-pytz python3-django-etesync-journal

%if 0%{?fedora}
BuildRequires: systemd-rpm-macros
%endif

%{?systemd_requires}

AutoReq:       no 
AutoReqProv:   no

%description
A skeleton app for running your own EteSync server

%prep
%setup -q -n server-%{version}

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}/usr/lib
mkdir -p %{buildroot}%{_sysconfdir}/%{name}
mkdir -p %{buildroot}%{_sysusersdir}

cat << EOF > %{buildroot}%{_bindir}/%{name}
#!/bin/sh

if [ "\$(id -un)" != "etesync" ];
then
        echo "Please run this script as user etesync"
        exit 0
fi

/usr/lib/etesync-server/manage.py \$@
EOF
chmod +x %{buildroot}%{_bindir}/%{name}

sed -i 's@/usr/bin/env python@/usr/bin/python3@' %{_builddir}/server-%{version}/manage.py

sed -i "s@secret.txt@/var/lib/etesync-server/secret.txt@;s@db.sqlite3@/var/lib/etesync-server/db.sqlite3@" "%{_builddir}/server-%{version}/%{name}.ini.example"
mv %{_builddir}/server-%{version}/%{name}.ini.example %{buildroot}%{_sysconfdir}/%{name}/%{name}.ini

cp -a %{_builddir}/server-%{version} %{buildroot}/usr/lib/%{name}
rm -rf %{buildroot}/usr/lib/%{name}/{example-configs,*.md,LICENSE}

%pre
getent group etesync >/dev/null || groupadd -r etesync
getent passwd etesync >/dev/null || \
    useradd -r -g etesync -d /usr/lib/etesync-server -s /sbin/nologin \
    -c "EteSync server" etesync 

%post
mkdir -p /var/lib/etesync-server
chown etesync:etesync /var/lib/etesync-server

%files
%{_bindir}/%{name}
/usr/lib/%{name}
%{_sysconfdir}/%{name}/%{name}.ini
%license LICENSE
%doc README.md

%changelog
* Wed Jan 29 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.3.0-1
- Update to version 0.3.0

* Tue Jan 21 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.2.2-1
- Update to version 0.2.2

* Sat Sep 28 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.2.1-1
- Update to version 0.2.1

* Mon May 20 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.2.0-1
- Update to version 0.2.0

* Sat May 04 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.1.0-2
- Add a system user
- Improve etesync-server script
- Make etesync-server create its default database to a proper directory

* Tue Apr 30 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.1.0-1
- Version 0.1.0
